$(function () {
  $('[data-toggle="popover"]').popover();
});

// $('.btn-query').on('mouseout', function() {
//   $('[data-toggle="popover"]').popover('hide');
// });

$('#tablist a').click(function (e) {
  e.preventDefault();
  $(this).tab('show');
});

$('#collapseQuery').on('shown.bs.collapse', function() {
    $('.querySwitch').text('收起筛选');
    $('.queryArrow').removeClass('fa-arrow-down').addClass('fa-arrow-up')
});

$('#collapseQuery').on('hidden.bs.collapse', function() {
    $('.querySwitch').text('展开筛选');
    $('.queryArrow').removeClass('fa-arrow-up').addClass('fa-arrow-down')
});

// $(document).on('click', '.add-food-cate', function(event) {
//   $(this).hide();
//   var i = $('.foodCateTemp').clone();
//   i.insertBefore('#mark').show()
//     .removeClass('foodCateTemp').addClass('foodCate');
//   i.find('input').attr({
//       name: 'foodName'
//     });
//   i.find('select').attr({
//       name: 'foodCate'
//     });
// });
//
// $(document).on('click', '.del-food-cate', function(event) {
//   var foodCate = $(this).parent('.foodCate');
//   var foodCateSize = $('.foodCate').siblings().length-1;
//   console.log(foodCateSize);
//   foodCate.each(function() {
//     if ( foodCateSize == 1 ) {
//       return false;
//     } else {
//       foodCate.remove();
//     };
//   });
// });
